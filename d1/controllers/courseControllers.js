const mongoose = require("mongoose");
const Course = require("../models/course.js");
const auth = require("../auth.js");


// Function for adding a course
// 2) Update the "addCourse" controller method to implement admin authentication for creating a course

//Activity Solution (s39)
module.exports.addCourse = (request) => {
	
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	if(isAdmin == true){
		let newCourse = new Course({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots: request.body.slots
		})
		return newCourse.save().then((newCourse, error) =>{
			if(error){
				return error;
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});	
	}
}


/*
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		
	})
	return newCourse.save().then((newCourse, error) =>{
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}
*/
// GET all course
module.exports.getAllCourse = () => {
	return Course.find({}).then(result =>{
		return result;
	})
}

// GET all Active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}

//GET specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result =>{
		return result;
	})
}

// UPDATING course
//newData.course.name
	// newData.request.body.name
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

// ARCHIVE course 
// Activity Solution (s40)

/*
1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
3. Process a PATCH request at the /courseId/archive route using postman to archive a course
*/

module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				
				isActive: newData.isActive
				
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}
















