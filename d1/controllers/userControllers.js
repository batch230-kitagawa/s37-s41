const User = require("../models/user");
const Course = require("../models/course")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqBody.mobileNumber,
		password: bcrypt.hashSync(reqBody.password, 10)
		// 10 - salt
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// function checkEmailExist
module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result =>{
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			// compareSync is a bcrypt function to comapre unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return{access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}

		}
	})
}

// s38 Activity Teacher Solution
/*module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody._id).then((result,err) => {
		if(err){
			return false;
		}
		else{
			result.password = "******";
			return result;
		}
	})
}
*/

// s38 Activity (My solution)
/*
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody._id).then(result => {
		if(result.length == 0){
			return false;
		}
		else{
			result.password = "";
			return result;
		}

	})
} 
*/

// S41
module.exports.getProfile = (request, response) =>{
	// will contain your decoded token
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "******";
		response.send(result);
	})
}

// Enroll feature
module.exports.enroll = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	let courseName = await Course.findById(request.body.courseId).then(result => result.name);

	let newData = {
		// User Id and email will be retrieve from the request header (request header contains the user token)
		userId: userData.id,
		email: userData.email,
		// Course Id will be retrieved from the request body
		courseId: request.body.courseId,
		courseName: courseName

	}
	console.log(newData);

	let isUserUpdated = await User.findById(newData.userId).then(user => {
		user.enrollments.push({
			courseId: newData.courseId,
			courseName: newData.courseName
			})
		
	

	return user.save()
	.then(result => {
		console.log(result);
		return true
	})
	.catch(error => {
		console.log(error);
		return false;
		})
	})
	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(newData.courseId).then(course => {

		course.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
	
	// Mini Activity
	// Create a condition that if the slots is already zero, no deduction of slot will happen and 
	// it should have a message in the terminal that the slot is already zero
	// Else if the slot is negative value, it should make the slot value to zero

	// Minus the slots available by 1
	//course.slots = course.slots - 1;
	if(course.slots == 0){
		console.log('User is not allowed to enroll due to no available slots');
	}
	else if(course.slots < 0){
		course.slots = 0;
	}
	else{
		course.slots -= 1;
	}
	

	return course.save()
	.then(result => {
		console.log(result);
		return true;
		})
	.catch(error => {
		console.log(error);
		return false
		})
	})
	console.log(isCourseUpdated);

	// Ternary operator
	(isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false)
	/*
	if(isUserUpdated == true && isCourseUpdated == true){
		response.send(true);
	}
	else{
	response.send(false);
	}

	*/

}

	







// Test Code to understand how auth.decode method works
module.exports.getToken = (request) => {
	return User.findById(request.body._id).then((result,err) => {
		if(err){
			return false;
		}
		else{
			const token = request.headers.authorization;
			console.log (token);

			const payload = auth.decode(token);
			return payload;
		}
	})
}
