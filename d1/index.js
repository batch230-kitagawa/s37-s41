/*
Terminal: 
npm init -y
npm install express
npm install mongoose
npm install cors
*/

// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes.js")


// to create an express server/application
const app = express();

// Middleware - allows to bridge our backend application (server) to our front end
// To allow cross origin resource sharing
app.use(cors());
// To read json objects
app.use(express.json());
// To read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch230.y9m8uao.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Kitagawa-Mongo DB Atlas")
	);


app.listen(process.env.PORT || 5000, () => {
	console.log(`API is now online on port ${process.env.PORT || 5000}`)
});


