const express = require('express');
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");


// Activity
// 1)Refractor the "course" route to implement user authentication for the admin when creating a course.
router.post("/create", auth.verify, (request, response) => {
	courseControllers.addCourse(request).then(resultFromController => response.send(
		resultFromController))
})

// Creating a course
/*
router.post("/create", (request, response) => {
	courseControllers.addCourse(request.body).then(resultFromController => response.send(
		resultFromController))
})
*/

//router.post("/create", auth.verify, courseControllers.addCourse)

// GET all courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(
		resultFromController))
})


// GET all ACTIVE courses
router.get("/ACTIVE", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromController => response.send(
		resultFromController))
})

// GET SPECIFIC course
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
})


router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


// Activity (s40)
router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const newData = {
		isActive: request.body.isActive, 	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.archiveCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})



module.exports = router;