const express = require('express');
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/register", (request,response) =>{
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/checkEmail", (request, response)=> {
	userControllers.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/login", (request, response)=> {
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

// s38 Activity
/*
router.post("/details", (request, response)=> {
	userControllers.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})
*/

router.get("/details", auth.verify, userControllers.getProfile);

router.post("/enroll", auth.verify, userControllers.enroll)

/*
router.post("/verify", (request, response)=> {
	userControllers.verifyUser(request.body).then(resultFromController => response.send(resultFromController))
})
*/

// Test Code to understand how auth.decode method works
router.post("/getToken", (request, response)=> {
	userControllers.getToken(request).then(resultFromController => response.send(resultFromController))
})

module.exports = router;